//SOAL 1
function teriak(){
    return "Halo Sanbers!";
}
console.log(teriak());

console.log("-----------------------------");

//SOAL 2
function kalikan(num1, num2){
    return num1*num2;
}
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)

console.log("-----------------------------");

//SOAL 3
function introduce(name, age, address, hobby){
    return "Nama saya " +  name + "," + " umur saya " + age + " tahun, " + "alamat saya di " + address 
    + ", dan saya punya hobby yaitu " + hobby + "!"
}

var name = "Rifki"
var age = 19
var address = "Perum Telaga Harapan H7/7, Bekasi, Jawa Barat"
var hobby = "Gaming & Soccer"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)